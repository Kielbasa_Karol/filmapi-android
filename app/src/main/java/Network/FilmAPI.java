package Network;

import Model.Movies;
import Model.Trailer;
import retrofit2.Call;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Kiełbasa Karol on 07.02.2017.
 */

public interface FilmAPI {

    String BASE_URL = "http://api.themoviedb.org/3/";
    /* Insert api key */
    String API_KEY = "";
    String PARAM_URL = "?api_key="+API_KEY+"&page=1";

    @GET("movie/top_rated/"+ PARAM_URL) Call<Movies> getTopRatedResults();

    @GET("movie/now_playing/"+ PARAM_URL) Call<Movies> getNewResults();

    @GET("movie/{id}/videos" + PARAM_URL) Call<Trailer> getTrailers(@Path("id") int groupId);
    class Factory {

        private static FilmAPI service;

        public static FilmAPI getIstance() {
            if (service == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                service = retrofit.create(FilmAPI.class);
                return service;
            } else {
                return service;
            }
        }
    }
}
