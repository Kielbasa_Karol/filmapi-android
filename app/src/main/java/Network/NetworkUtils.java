package Network;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by Kiełbasa Karol on 07.02.2017.
 */

public final class NetworkUtils {

    final static String IMAGE_URL = "http://image.tmdb.org/t/p/";
    final static String SIZE = "w342";

    public static URL buildImageURL(String imageAdress){
        Uri uriToUrl = Uri.parse(IMAGE_URL).buildUpon()
                .appendPath(SIZE)
                .appendEncodedPath(imageAdress)
                .build();
        return getURLfromUri(uriToUrl);
    }

    private static URL getURLfromUri(Uri uri){
        URL buildedURL = null;
        try {
            buildedURL = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return buildedURL;
    }

    public static void watchTrailer(String key,View v) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + key));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + key));
        try {
            v.getContext().startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            v.getContext().startActivity(webIntent);
        }
    }
}