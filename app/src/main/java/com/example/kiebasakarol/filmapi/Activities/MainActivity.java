package com.example.kiebasakarol.filmapi.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.kiebasakarol.filmapi.R;

import java.util.ArrayList;

import Adapter.MovieAdapter;
import Model.Movies;
import Model.Result;
import Network.FilmAPI;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Result> movies;

    @BindView(R.id.poster_recycle_view_top_rated) RecyclerView recyclerViewTopRated;
    private MovieAdapter movieAdapterTopRated;

    @BindView(R.id.poster_recycle_view_now_playing) RecyclerView recyclerViewNowPlaying;
    private MovieAdapter movieAdapterNowPlaying;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initTopRatedAdaperAndRecyclerView();
        initNowPlayingAdaperAndRecyclerView();

        loadData();
    }

    private void initNowPlayingAdaperAndRecyclerView() {
        movieAdapterNowPlaying = new MovieAdapter(this);

        recyclerViewNowPlaying.setLayoutManager(new LinearLayoutManager(recyclerViewNowPlaying.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewNowPlaying.setHasFixedSize(true);
        recyclerViewNowPlaying.setAdapter(movieAdapterNowPlaying);

    }

    private void initTopRatedAdaperAndRecyclerView() {
        movieAdapterTopRated = new MovieAdapter(this);

        recyclerViewTopRated.setLayoutManager(new LinearLayoutManager(recyclerViewTopRated.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewTopRated.setHasFixedSize(true);
        recyclerViewTopRated.setAdapter(movieAdapterTopRated);
    }

    public void loadData() {
        FilmAPI.Factory.getIstance().getTopRatedResults().enqueue(new Callback<Movies>() {
            @Override
            public void onResponse(Response<Movies> response) {
                movies = new ArrayList<>();
                movies.addAll(response.body().getResults());
                movieAdapterTopRated.initMovies(movies);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Failed", t.getMessage());
            }
        });

        FilmAPI.Factory.getIstance().getNewResults().enqueue(new Callback<Movies>() {
            @Override
            public void onResponse(Response<Movies> response) {
                movies = new ArrayList<>();
                movies.addAll(response.body().getResults());
                movieAdapterNowPlaying.initMovies(movies);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Failed", t.getMessage());
            }
        });
    }
}


