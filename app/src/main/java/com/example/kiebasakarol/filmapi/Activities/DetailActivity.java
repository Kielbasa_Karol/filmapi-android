package com.example.kiebasakarol.filmapi.Activities;

import android.content.Intent;
import android.graphics.Movie;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kiebasakarol.filmapi.R;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

import Adapter.MovieAdapter;
import Adapter.TrailerAdapter;
import Model.Movies;
import Model.Result;
import Model.ResultsTrailer;
import Model.Trailer;
import Network.FilmAPI;
import Network.NetworkUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.poster_detail) ImageView moviePoster;
    @BindView(R.id.title_detail) TextView movieTitle;
    @BindView(R.id.rate_detail) TextView rate;
    @BindView(R.id.description_detail) TextView description;
    @BindView(R.id.poster_recycle_view_trailers) RecyclerView recyclerViewtrailsers;
    private TrailerAdapter trailerAdapter;
    private ArrayList<ResultsTrailer> trailers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        initTrailerAdapter();

        loadDataFromPreviousIntent();
        loadData();
    }

    private void initTrailerAdapter() {
        trailerAdapter = new TrailerAdapter(this);

        recyclerViewtrailsers.setLayoutManager(new LinearLayoutManager(recyclerViewtrailsers.getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerViewtrailsers.setHasFixedSize(true);
        recyclerViewtrailsers.setAdapter(trailerAdapter);
    }

    private void loadDataFromPreviousIntent() {
        Intent previousIntent = getIntent();
        if (previousIntent != null) {
            Bundle data  = getIntent().getExtras();
            Result movie = (Result) data.getParcelable("movie");

            if(previousIntent.hasExtra("movie")){
                movieTitle.setText(movie.getTitle());
                rate.setText(""+movie.getVoteAverage()+"/10");
                description.setText(movie.getOverview());
                URL imageUrl = NetworkUtils.buildImageURL(movie.getPosterPath());
                Picasso.with(this).load(imageUrl.toString()).into(moviePoster);
            }
        }
    }

    public void loadData() {
        Intent previousIntent = getIntent();
        if (previousIntent != null) {
            Bundle data  = getIntent().getExtras();
            Result movie = (Result) data.getParcelable("movie");

            if(previousIntent.hasExtra("movie")){
                FilmAPI.Factory.getIstance().getTrailers(movie.getId()).enqueue(new Callback<Trailer>() {
                    @Override
                    public void onResponse(Response<Trailer> response) {
                        trailers =  new ArrayList<>();
                        trailers.addAll(response.body().getResults());
                        trailerAdapter.initTrailers(trailers);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.e("Failed", t.getMessage());
                    }
                });
            }
        }

    }
}
