package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kiebasakarol.filmapi.Activities.DetailActivity;
import com.example.kiebasakarol.filmapi.R;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

import Model.Result;
import Network.NetworkUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kiełbasa Karol on 06.02.2017.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {

    private static final String LOG_TAG = MovieAdapter.class.getSimpleName();

    private Context context;
    private ArrayList<Result> movies;

    public MovieAdapter(Context context)
    {
        this.context = context;
    }

    public void initMovies(ArrayList<Result> movies){
        this.movies = movies;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.poster) ImageView moviePoster;
        @BindView(R.id.title) TextView movieTitle;
        @BindView(R.id.year) TextView year;

        public MyViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this,itemView);

            moviePoster.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Result result = movies.get(getAdapterPosition());
                    Intent toDetail = new Intent(v.getContext(), DetailActivity.class);
                    toDetail.putExtra("movie",result);
                    v.getContext().startActivity(toDetail);
                }
            });
        }

        public void loadImage() {
            if(movies.size()>0) {
                URL imageUrl = NetworkUtils.buildImageURL(movies.get(getAdapterPosition()).getPosterPath());
                Picasso.with(context).load(imageUrl.toString()).into(moviePoster);
            }
        }

        public void loadData() {
            if(movies.size()>0) {
                movieTitle.setText(movies.get(getAdapterPosition()).getTitle());

                String yearToFormat = movies.get(getAdapterPosition()).getReleaseDate();
                year.setText(formatYear(yearToFormat));
            }
        }
    }

    private String formatYear(String yearToFormat) {
        return yearToFormat.substring(0,4);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutID = R.layout.poster_layout;

        LayoutInflater inflater = LayoutInflater.from(context);
        
        View view = inflater.inflate(layoutID,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.loadImage();
        holder.loadData();
    }

    @Override
    public int getItemCount() {
        if(movies == null) return 0;
        else return movies.size();
    }
}
