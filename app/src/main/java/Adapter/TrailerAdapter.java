package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kiebasakarol.filmapi.R;

import java.util.ArrayList;

import Model.ResultsTrailer;
import Network.NetworkUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kiełbasa Karol on 13.02.2017.
 */

public class TrailerAdapter extends RecyclerView.Adapter<TrailerAdapter.MyViewHolder> {

    private ArrayList<ResultsTrailer> trailers;
    private Context context;

    public TrailerAdapter(Context context){
        this.context = context;
    }

    public void initTrailers(ArrayList<ResultsTrailer> trailers){
        this.trailers = trailers;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.play_trailer) ImageView playImageView;
        @BindView(R.id.trailer_TV) TextView trailerTextView;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void loadData() {
            if(trailers.size() > 0){
                trailerTextView.setText(trailers.get(getAdapterPosition()).getName());
                playImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NetworkUtils.watchTrailer(trailers.get(getAdapterPosition()).getKey(),v);
                    }
                });
            }
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.trailer_layout,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.loadData();
    }

    @Override
    public int getItemCount() {
        if(trailers == null) return 0;
        else return trailers.size();
    }
}
