# README #

Application shows top rated and now playing movies. User is able to click on poster and see more details about movie. Movies are shown in Recycler View.

App uses a data from API (https://www.themoviedb.org/documentation/api) 
Unique key is required. To create one you have to register here: https://www.themoviedb.org/account/signup?language=en-EN
Then paste the key to code (in FilmAPI interface to Sring API_KEY)


# LIBRARIES #

* ButterKnife - https://github.com/JakeWharton/butterknife
* Retrofit - https://square.github.io/retrofit/
* Picasso - http://square.github.io/picasso/

# SCREENS #

![android main.png](https://bitbucket.org/repo/ao7GBR/images/1035800895-android%20main.png)
![andorid detail.png](https://bitbucket.org/repo/ao7GBR/images/2503760968-andorid%20detail.png)